import os
import sys
import json
import hashlib
import argparse
import subprocess

plugin_list = [
        { "name": "psscan", "type":"json"},
        { "name": "pslist", "type":"json"},
        { "name": "pstree", "type":None},
#        { "name": "kdbgscan", "type":"json"},
#        { "name": "kpcrscan", "type":"json"},
#        { "name": "dlldump", "type":"json"},
        { "name": "dlllist", "type":"json"},
        { "name": "consoles", "type":"json"},
        { "name": "connections", "type":"json"},
        { "name": "connscan", "type":"json"},
        { "name": "netscan", "type":"json"},
        { "name": "sockets", "type":"json"},
        { "name": "hivescan", "type":"json"},
        { "name": "hivelist", "type":"json"},
        { "name": "svcscan", "type":"json"},
        { "name": "cmdline", "type":"json"},
        { "name": "filescan", "type":"json"},
        { "name": "hashdump", "type":"json"},
        { "name": "envars", "type":"json"},
        { "name": "iehistory", "type":"json"},
        { "name": "malconfscan", "type":"json"},
#        { "name": "mftparser", "type":"json"},
        ]

gSaveDir = "result"

def proc( image_file, plugin, profile=None, type="json" ) :
    global gSaveDir

    output_file = os.path.join( gSaveDir, "%s.dat"%(plugin) )
    if os.path.exists(output_file) == False :
        if profile == None :
            cmd = "volatility -f %s %s --output=json > %s"%(image_file, plugin, output_file)
        else :
            cmd = "volatility -f %s --profile=%s %s --output=json > %s"%(image_file, profile, plugin, output_file)

        if type != "json" :
            cmd = cmd.replace("--output=json","")

        os.system(cmd)
    try :
        data = json.load( open(output_file) )
    except :
        data = open(output_file).read()

    print( "#### Plug-in [%s] ####"%(plugin) )
    print( json.dumps( data, indent=2 ) )
    return(data)

def custom_makedirs(path):
    if not os.path.isdir(path):
        os.makedirs(path)

def vol_proc( filename ) :
    global plugin_list
    global gSaveDir

    with open(filename, 'rb') as f:
        checksum = hashlib.md5(f.read()).hexdigest()
        gSaveDir = os.path.join(gSaveDir, checksum)

    custom_makedirs(gSaveDir)

    r = proc(filename,"imageinfo")
    profile = r["rows"][0][0].split(",")[0]

    for item in plugin_list :
        r = proc(filename,item["name"], profile, item["type"])
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='VolatilityTool')

    parser.add_argument('image', help='image')

    args = parser.parse_args()
    vol_proc( args.image )
  
