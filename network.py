
# https://github.com/volatilityfoundation/volatility/wiki/Command-Reference-Mal

import os
import re
import sys
import json
import copy
import hashlib
import argparse
import subprocess

gSaveDir = "result"
gDonePID = {"procdump": [], "memdump": [], "malfind":[]}

def proc( image_file, plugin, profile, pid ) :
    global gSaveDir

    if plugin in gDonePID :
        if pid in gDonePID[plugin] :
            return
        gDonePID[plugin].append( pid )

    check_file = ""
    if plugin == "procdump" :
        check_file = os.path.join( gSaveDir, "executable.%d.exe"%(pid))
    elif plugin == "memdump":
        check_file = os.path.join( gSaveDir, "%d.dmp"%(pid))

    print(check_file)
    if (check_file != "") and (os.path.exists( check_file ) == True) :
        return

    if plugin == "malfind" :
        result_name = os.path.join(gSaveDir, "malfind_%d.log"%(pid))
        cmd = "volatility -f %s --profile=%s %s -p %d --dump-dir %s --output=json > %s"%(image_file, profile, plugin, pid, gSaveDir, result_name)
        print(cmd)
        os.system(cmd)

        if (os.path.getsize(result_name) == 0) :
            os.remove( result_name )
            return

        mal_data = json.load( open(result_name) )
        result_name = os.path.join(gSaveDir, "impscan_%d.log"%(pid))
        # ["Process", "Pid", "Address", "VadTag", "Protection", "Flags", "Data"]
        address = mal_data["rows"][0][2]
        cmd = "volatility -f %s --profile=%s %s -p %d -b %d --output=json > %s"%(image_file, profile, "impscan", pid, address, result_name)
        print(cmd)
        os.system(cmd)
    else :
        cmd = "volatility -f %s --profile=%s %s -p %d --dump-dir %s"%(image_file, profile, plugin, pid, gSaveDir)
        print(cmd)
        os.system(cmd)

    result_name = os.path.join(gSaveDir, "apihook_%d.log"%(pid))
    cmd = "volatility -f %s --profile=%s %s -p %d --output=json > %s"%(image_file, profile, "apihooks", pid, result_name)
    print(cmd)
    os.system(cmd)

    return

def custom_makedirs(path):
    if not os.path.isdir(path):
        os.makedirs(path)

def vol_proc( filename ) :
    global plugin_list
    global gSaveDir

    with open(filename, 'rb') as f:
        hash_v = hashlib.md5(f.read()).hexdigest()
    print( "#### HASH [%s]"%(hash_v))

    result = {}

    gSaveDir = os.path.join( "./result", hash_v, "images" )
    custom_makedirs( gSaveDir )

    ########################
    #  imageinfo.dat
    target_file = os.path.join( "./result", hash_v, "imageinfo.dat" )
    r = json.load( open(target_file) )
    profile = r["rows"][0][0].split(",")[0]
    print(profile)
    #
    ########################

    ########################
    #  connscan.dat
    target_file = os.path.join( "./result", hash_v, "connscan.dat" )
    if (os.path.getsize(target_file) > 0) :
        connscan = json.load( open(target_file) )
        # [u'Offset(P)', u'LocalAddress', u'RemoteAddress', u'PID']
        for item in connscan["rows"] :
            if item[3] not in result :
                result[item[3]] = {"type": "CONNECT", "ip":[], "cmdline":[], "offset": "", "parent": {}}
            if item[2] not in result[item[3]]["ip"] :
                result[item[3]]["ip"].append( item[2] )
    #
    ########################

    ########################
    #  netscan.dat
    target_file = os.path.join( "./result", hash_v, "netscan.dat" )
    if (os.path.getsize(target_file) > 0) :
        connscan = json.load( open(target_file) )
        #"columns": [ "Offset(P)", "Proto", "LocalAddr", "ForeignAddr", "State", "PID", "Owner", "Created" ]
        for item in connscan["rows"] :
            if "ESTABLISHED" == item[4] :
                if ("127.0.0.1" in item[3]) or ("*" in item[3]) or ("0.0.0.0" in item[3]) or (item[3].count(":") >= 2) :
                    continue
            elif "LISTENING" != item[4] :
                    continue

            if item[5] not in result :
                result[item[5]] = {"type": item[4], "ip":[], "cmdline":[], "offset": "", "parent": {}}
            if item[3] not in result[item[5]]["ip"] :
                result[item[5]]["ip"].append( item[3] )
    #
    ########################

    ########################
    #  cmdline.dat
    target_file = os.path.join( "./result", hash_v, "cmdline.dat" )
    cmdline = json.load( open(target_file) )
    # ["Process", "PID", "CommandLine"
    for item in cmdline["rows"] :
        if item[1] not in result :
            continue

        #print("-------")
        #print(item)
        if item[0] not in result[item[1]]["cmdline"] :
            result[item[1]]["cmdline"].append( item[0] )
    #
    ########################

    ########################
    #  pslist.dat
    plist = {}
    target_file = os.path.join( "./result", hash_v, "pslist.dat" )
    cmdline = json.load( open(target_file) )
    for item in cmdline["rows"] :
        # "Offset(V)", "Name", "PID", "PPID", "Thds", "Hnds", "Sess", "Wow64", "Start", "Exit"
        try :
            pid = int(item[2])
        except :
            continue
        if item[2] not in plist :
            plist[pid] = {"parent":int(item[3]), "offset":item[0], "name": item[1] }
    for pid in result :
        if pid in plist :
            result[pid]["offset"] =  plist[pid]['offset']
            ppid = plist[pid]['parent']
            cur = result[pid]["parent"]
            while ppid != 0 :
                #cur = {'name': plist[ppid]['name'], 'parent': {}, 'offset': plist[ppid]['offset']}
                try :
                    cur["name"] = plist[ppid]['name']
                except :
                    cur["name"] = "None"
                try :
                    cur["offse"] = plist[ppid]['offset']
                except :
                    cur["offse"] = "None"
                cur["pid"] = ppid
                cur["parent"] = {}

                cur = cur["parent"]
                try :
                    ppid = plist[ppid]['parent']
                except :
                    break
    #
    ########################

    ########################
    #  dump
    for pid in result :
        proc( filename, "procdump", profile, pid )
        proc( filename, "memdump", profile, pid )
        proc( filename, "malfind", profile, pid )

        cur = result[pid]["parent"]
        if "pid" not in cur :
            continue
        while cur["pid"] != 0 :
            proc( filename, "procdump", profile, cur["pid"] )
            proc( filename, "memdump", profile, cur["pid"] )
            proc( filename, "malfind", profile, cur["pid"] )

            cur = cur["parent"]
            if "pid" not in cur :
                break
    #
    ########################

    print( json.dumps(result, indent=2) )
    target_file = os.path.join( "./result", hash_v, "conn_trace.dat" )
    f = open(target_file, "w")
    json.dump( result, f, indent=2)
    f.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='VolatilityTool')

    parser.add_argument('image', help='image')

    args = parser.parse_args()
    vol_proc( args.image )
